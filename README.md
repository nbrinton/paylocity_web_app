# Paylocity Employee Benefits Manager Solution

This project is my solution to the Paylocity hiring problem statement. My solution uses
the Flask web framework to create a web app that multiple companies can create users on
and whose users can then login and add, edit, or delete employees for that company. The
primary feature of the website is to calculate and display the cost of benefits for each 
employee, and thus, once you sign in, the index page is a list of all of the employees
in order.

## Getting Started

Firstly you need to make sure that you have git installed on your system so that you can
clone the repository onto your machine. Or should you choose, you can simply download the
raw source code from the repository on Bitbucket directly.

After either cloning or downloading the repository, change into the "paylocity_flask_app"
directory within the root directory of the repository. This contains the solution. 

To run the program you must first activate the virtual environment with the command:
~~~
	. venv/bin/activate
~~~
When you're done using the virtual environment, deactivate it using the command:
~~~
	deactivate
~~~

Do not deactivate the virtual environment until you are completely done running the
program!

To run the program now, all you should have to do is give the shell script "run.sh"
executable privileges and then run it using these two commands:
~~~
	chmod +x run.sh
	./run.sh
~~~

This should run an instance of the app on the localhost port 5000. Open up a web browser
of your choice and in the url enter:
~~~
	localhost:5000
~~~

This will take you to the index page of the website. From here you can use the website
to your liking.



### Prerequisites

I haven't gotten a vagrant file for this to work properly yet, so at the moment you will
most likely need a Linux operating system to easily get this project up and running. Also,
see "DEPENDENCIES" below should you choose not to use the virtual environment for some
reason.

## Running the tests

I haven't had the time to learn how to properly implement unit tests for my website. There
is a basic setup for running them that I put in there from the Flask "flaskr" demo, but 
there aren't any actual unit tests to run as of now.

## Authors

* **Nathan Brinton**

## DEPENDENCIES:

All of the dependcies for this project should be properly installed within the virtual 
environment. If you should choose to not use the virtual environment, for some odd reason,
then you will, however, need to make sure to install the following onto your system:

~~~
sudo apt-get install sqlite3 (Shouldn't need this due to function within paylocity_app.py)
sudo apt-get install python3-pip
sudo pip install Flask
sudo pip install pytest
sudo pip install Flask-WTF
sudo pip install passlib
~~~

## BUILDING AND RUNNING:

To manually initialize the databse, enter:
~~~
	sqlite3 /tmp/paylocity_app.db < schema.sql
~~~

To initialize the databse through the program, enter (order of commands in order to do this is outlined in: initdb_command_order.png
~~~
	flask initdb
~~~

Since the application is created as a package, you have to install it using pip 
by entering this from within the root directory paylocity_flask_app:
~~~
	pip install --editable .
~~~

Then set the environment variables for the app by entering:
~~~
	export FLASK_APP=paylocit_app.paylocity_app
	export FLASK_DEBUG=true
~~~

Initialize the database by running this in the root directory of this project:
~~~
	flask initdb
~~~

Run the app by entering:
~~~
	flask run
~~~

If you get an exception later on stating that a table cannot be found, check that you did 
execute the initdb command and that your table names are correct (singular vs. plural, for example).

Run the unit tests by entering: (Note: there are currently no unit tests implements)
~~~
	pytest
~~~

Also, by having added a few extra lines to the setup.py and creating setup.cfg, we can run the tests by entering:
~~~
	python setup.py test
~~~


## FUTURE DEVELOPMENT:

1. "Edit" Functionality
	* WTForms Functionality
		Currently I cannot figure out how to get the edit forms to fill with the old values using wtforms so I'm 
		not using them for this. However, this means that the validators aren't there and that's a security risk.
	* Vulnerability: Editing an employee's ID to one that already exists
		I currently have this check in the add employee as well as in the register code, but I don't have this in 
		the edit code yet. You should only be able to edit the employee whose name you clicked on and not another
		one indirectly.

2. Vulnerability: Company ID and registering a new user
	* Currently anyone can register with any company so long as they know that company's ID number. I would like to
	eventually create some sort of check (be it a password, email verification, what have you) so that you can 
	only create a user account for a company if you have permission to do so. This will likely involve implementing
	the full company table functionality that I currently have commented out and not fully implemented.

3. Create vagrant file for easy setup
	* I would like to create a nice vagrant file for my project so that other people can easily create my development
	environment for viewing or editing purposes.

4. Helper functions
	* I feel like there's quite a bit of duplicate code which is bad practice and just uneccessary. I'm not sure how
	much I'm going to worry about it for now because it fully works and I don't want to mess it up, but it would be
	nice to make my code more robust in this fashion.

5. User Edit page
	* Currently you can only edit employees and not the actual admin users. I would like to add the functionality for
	users to edit their information (albeit the very little amount of information that they can actually modify).

6. Add employees from a csv or other file format 
	* Currently a user has to enter every employee manually. This is a very unlikely scenario and so it would be a nice
	feature for users to be able to add a slew of employees from a selected file (probably csv).

7. Strip strings properly
	* I'm not exactly sure if I do any real string stripping on any of my forms or anything, and this is typically good
	practice to avoid very frustrating consequences to either developers or users. (Especially fix the period existing
	even when a person does not list a middle initial).

8. Password requirements
	* I have no idea at the moment how you would implement this, but it would be a nice feature to have minimum password
	requirements such as being a minimum length, containing a special character and number, etc.

9. Styling touch-up
	* The styling is fairly nice (and certainly much nicer than it was when I started), but it would be nice to finesse
	the styling and really make it perfect.

10. Company table functionality
	* Currently I am not actually using a separate company table combined with JOIN statements, and it isn't super
	necessary at the moment and it adds an extra layer of complexity which opens you to the possibility of more bugs.
	This would be a nice and realistic thing to add to the website, however, and opens the door for more functionality.

11. Better handling of salary
	* Currently the salary accepts a floating point number, but it doesn't work if the user enters in commas, and I would
	like it to be able to strip those out without any issue.

12. Bug: Edit form salary field doesn't have validators
	* Since I haven't been able to implement WTForms for the edit form, it isn't validating before submitting and a user
	can use commas in their salary field and when they try to edit the employee again or view them it will break from
	trying to do float stuff on a string type.


##PRIMARY SOURCES:

###FLASK:

* [Flask Installation](http://flask.readthedocs.io/en/latest/installation/)
* [Flask Quickstart](http://flask.readthedocs.io/en/latest/quickstart/)
* [Flask Tutorial: Flaskr](http://flask.readthedocs.io/en/latest/tutorial/)

###WTFORMS:

* [Flask Quickstart: Creating WTForms](https://flask-wtf.readthedocs.io/en/stable/quickstart.html#creating-forms)
* [WTForms Fields](http://wtforms.simplecodes.com/docs/0.6.1/fields.html)
* [WTForms Validators](http://wtforms.readthedocs.io/en/latest/validators.html)
* [Form Validation with WTForms](http://flask.pocoo.org/docs/0.12/patterns/wtforms/)

###JINJA2:

* [Jinja2 Template Designer Documentation](http://jinja.pocoo.org/docs/2.10/templates/)
* [Jinja2 Documentation](http://jinja.pocoo.org/docs/2.10/)

###SQLITE

* [SQL as Understood by SQLite](https://sqlite.org/lang.html)

###BOOTSTRAP

* [Bootstrap Home](https://getbootstrap.com/)
* [Bootstrap Getting Started](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [Bootstrap Badges](https://getbootstrap.com/docs/4.0/components/badge/)
* [Bootstrap Lists](https://getbootstrap.com/docs/4.0/components/list-group/)
* [Bootstrap Navbar](https://getbootstrap.com/docs/4.0/components/navbar/)
* [Bootstrap Jumbotron](https://getbootstrap.com/docs/4.0/components/jumbotron/)

### USEFUL REFERENCE TUTORIAL VIDEOS

* [Traversy Media Video 1](https://www.youtube.com/watch?v=zRwy8gtgJ1A)
* [Traversy Media Video 2](https://www.youtube.com/watch?v=addnlzdSQs4&t=1209s)

## MISC SOURCES:

* [Install Venv for Python 3](https://gist.github.com/Geoyi/d9fab4f609e9f75941946be45000632b)
* [Python two decimal display for a floating point number](https://stackoverflow.com/questions/455612/limiting-floats-to-two-decimal-points)
* [Python for-loop iterator help](https://stackoverflow.com/questions/17691838/range-in-jinja2-inside-a-for-loop)
* [Python line continuation](https://stackoverflow.com/questions/4172448/is-it-possible-to-break-a-long-line-to-multiple-lines-in-python)
* [Insert a python variable into an SQLite query](https://stackoverflow.com/questions/902408/how-to-use-variables-in-sql-statement-in-python)
* [Flask Dynamic URL Building tutorial video](https://www.youtube.com/watch?v=WijSHvC18ro)
* [HTML Button Link](https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link)
* [Database update clause with string replacement](https://stackoverflow.com/questions/16322031/python-variable-replacement-in-sqlite3-insert-statement)
* [Python string formatting](https://stackoverflow.com/questions/5082452/python-string-formatting-vs-format)
* [SQLite update clause](https://www.tutorialspoint.com/sqlite/sqlite_update_query.htm)
* [SQLite documentation](https://sqlite.org/lang.html)
* [Jinja2 Comments](https://stackoverflow.com/questions/13562222/jinja2-inline-comments)
* [SQL Order of "WHERE" and "ORDER BY" clauses](https://www.techonthenet.com/sql/order_by.php)
* [SQLite Foreign Keys](https://sqlite.org/foreignkeys.html)
* [Flask/Jinja2 Template request.path for identifying current page](https://stackoverflow.com/questions/8676455/flask-current-page-in-request-variable)
* [Centering buttons in a div in Boostrap](https://stackoverflow.com/questions/22578853/how-to-center-buttons-in-twitter-bootstrap-3)
* [Bootstrap button group](https://stackoverflow.com/questions/11832250/css-bootstrap-display-button-inline-with-text)
* [Removing irritating collapsing](https://stackoverflow.com/questions/20219796/bootstrap-collapse-menu-disappears-when-resizing-screen)
* [Install Passlib](http://passlib.readthedocs.io/en/stable/install.html)
* [Currency Format](https://stackoverflow.com/questions/320929/currency-formatting-in-python/3393776)
* [Nice README format](https://gist.githubusercontent.com/PurpleBooth/109311bb0361f32d87a2/raw/824da51d0763e6855c338cc8107b2ff890e7dd43/README-Template.md)
* [MarkDown cheat sheet](https://en.support.wordpress.com/markdown-quick-reference/)
