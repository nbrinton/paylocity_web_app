#!/bin/bash

results="install_summary.txt"
installs=( "git" "vim" "vim-gnome" )
usage="Usage: ./setup.sh\n\t--basic\t: installs only the bare bones (best for an already well established system.\n\t--run\t: run the program after installation"
run=0
basic=0

echo "INSTALLING PAYLOCITY EMPLOYEE MANAGER SOLUTION"
echo "INSTALLING PAYLOCITY EMPLOYEE MANAGER SOLUTION" > $results

if [ $# -gt 2 ]; then
	echo "Invalid number of arguments:"
	echo -e $usage
fi

if [ $# -eq 1 ]; then
	if [ "$1" == "--basic" ]; then
		basic=1
		echo "BASIC INSTALLATION:"
		echo "BASIC INSTALLATION:" >> $results
		installs=( "git" )
	elif [ "$1" == "--run" ]; then
		echo "FULL INSTALLATION:"
		echo "FULL INSTALLATION:" >> $results
		run=1
	else
		echo "Invalid arguments:"
		echo -e $usage
		exit
	fi
elif [ $# -eq 2 ]; then
	if [ "$1" == "--basic" ] && [ "$2" == "--run" ]; then
		basic=1
		echo "BASIC INSTALLATION:"
		echo "BASIC INSTALLATION:" >> $results
		installs=( "git" )
		run=1
	elif [ "$1" == "--run" ] && [ "$2" == "--basic" ]; then
		basic=1
		echo "BASIC INSTALLATION:"
		echo "BASIC INSTALLATION:" >> $results
		installs=( "git" )
		run=1
	else
		echo "Invalid arguments:"
		echo -e $usage
		exit
	fi
	
fi

for i in "${installs[@]}"
do
	echo "Installing "$i
	echo "Installing "$i >> $results
	sudo apt-get install -y $i
	if [ $? -eq 0 ]; then
		echo "OK"
		echo "OK" >> $results
	else
		echo "FAIL"
		echo "FAIL" >> $results
	fi
done

echo "Cloning git repository"
echo "Cloning git repository" >> $results
git clone https://nbrinton@bitbucket.org/nbrinton/paylocity_web_app.git
if [ $? -eq 0 ]; then
	echo "OK"
	echo "OK" >> $results
else
	echo "FAIL"
	echo "FAIL" >> $results
fi

echo "Changing into solution directory"
echo "Changing into solution directory" >> $results
cd paylocity_web_app/paylocity_flask_app
if [ $? -eq 0 ]; then
	cd -
	echo "OK"
	echo "OK" >> $results
	cd -
else
	cd -
	echo "FAIL"
	echo "FAIL" >> $results
	cd -
fi

cd -
echo "Activating virtual environment (venv)"
echo "Activating virtual environment (venv)" >> $results
cd -
. venv/bin/activate
if [ $? -eq 0 ]; then
	cd -
	echo "OK"
	echo "OK" >> $results
	cd -
else
	cd -
	echo "FAIL"
	echo "FAIL" >> $results
	cd -
fi

if [ $run -eq 1 ]; then
	cd -
	echo "Running"
	echo "Running" >> $results
	cd -
	chmod +x run.sh
	if [ $? -eq 0 ]; then
		cd -
		echo "OK"
		echo "OK" >> $results
		cd -
	else
		cd -
		echo "FAIL"
		echo "FAIL" >> $results
		cd -
	fi
	./run.sh
fi

if [ $run -eq 1 ]; then
	cd -
	echo "Deactivating virtual environment (venv)"
	echo "Deactivating virtual environment (venv)" >> $results
	cd -
	deactivate
	if [ $? -eq 0 ]; then
		cd -
		echo "OK"
		echo "OK" >> $results
		cd -
	else
		cd -
		echo "FAIL"
		echo "FAIL" >> $results
		cd -
	fi
fi

cd -
echo "Finished"
echo "Finished" >> $results


