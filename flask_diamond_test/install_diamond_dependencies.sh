#!/bin/bash

results="install_results.txt"

function myprint {
	echo $1
	echo $1 >> $results
}

function result {
	if [ $? -eq 0 ]; then
		myprint "OK"
	else
		myprint "FAIL"
	fi
}

myprint  "START: Installing dependecies for Flask-Diamond"

# Install primary Flask-Diamond system dependencies
myprint "Installing primary Flask-Diamond system dependencies (including pip package manager)"
sudo apt-get install python python-dev python-pip build-essential
result

myprint "Installing sqlite-dev"
#sudo apt-get install sqlite-dev #>> $results
sudo apt-get install libsqlite3-dev #>> $results
result

myprint "Upgrading pip"
sudo pip install --upgrade pip #>> $results
result

myprint "upgrading virtualenv"
sudo pip install --upgrade virtualenv #>> $results
result

myprint "Installing virtualenvwrapper"
sudo pip install virtualenvwrapper #>> $results
result

myprint "DONE"
