from setuptools import setup

setup(
    name='paylocity_app',
    packages=['paylocity_app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)
